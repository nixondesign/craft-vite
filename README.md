# Craft Vite

This module provides integration with Craft CMS and Vite.

## Install

### Requirements

- Craft CMS 4

```
composer require nixondesign/vite
```

To load the module within a Craft CMS app add the module to your `config/app.php` file.

```php
<?php

use craft\helpers\App;

return [
    modules' => [
        'vite' => [
            'class' => \nixondesign\vite\Module::class,
            'components' => [
                'vite' => [
                    'class' => \nixondesign\vite\Service::class,
                    'manifestPath' => App::env('VITE_MANIFEST_PATH'),
                    'viteServerUrlPublic' => App::env('VITE_SERVER_URL_PUBLIC'),
                    'vitePublicUrl' => App::env('VITE_PUBLIC_URL'),
                    'vitePublicPath' => App::env('VITE_PUBLIC_PATH'),
                ],
            ],
        ],
    ],

    'bootstrap' => [
        'vite',
    ],
];
```
