## 0.4.3 - 16-03-2023

- Use server path when inlining assets specified using relative path.

## 0.4.2 - 16-03-2023

- Set different cache duration when dev server is running.

## 0.4.1 - 30-11-2022

- Fix handling of both paths and absolute URLs.

## 0.4.0 - 30-11-2022

- Fix error when file doesn't exist.

## 0.3.0 - 01-08-2022

- Make cache key configurable.
- Remove cache clearing utility. This should now be handled within the application.

## 0.2.0 - 01-08-2022

- Explicitly output the Vite client script when the dev server is running (2970d5e8b1b9d41993ff14529679acc0c7ba1c55).

## 0.1.0 - 29-07-2022

- Initial release.
