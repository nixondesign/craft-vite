<?php

namespace nixondesign\vite;

use Craft;
use craft\base\Component;
use craft\helpers\App;
use craft\helpers\ArrayHelper;
use craft\helpers\Html;
use craft\helpers\Json;
use craft\helpers\Template;
use craft\helpers\UrlHelper;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Illuminate\Support\Collection;
use Throwable;
use Twig\Markup;
use yii\caching\ChainedDependency;
use yii\caching\FileDependency;
use yii\caching\TagDependency;

/**
 * @property-read array $manifest
 * @property-read bool $isDevServerRunning
 */
class Service extends Component
{
    /**
     * @var int|null The duration to cache files for in seconds.
     */
    public ?int $cacheDuration = 0;

    /**
     * @var int|null The duration to cache files for in seconds.
     */
    public ?int $devModeCacheDuration = 1;

    /**
     * @var string The path to Vite's manifest file.
     */
    public string $manifestPath = '';

    /**
     * @var string The URL to the public directory.
     */
    public string $vitePublicUrl = '';

    /**
     * @var string The path to the public directory.
     */
    public string $vitePublicPath = '';

    /**
     * @var string The URL to the Vite dev server.
     */
    public string $viteServerUrlPublic = '';

    /**
     * @var string The path to the Vite client.
     */
    public string $viteClientPath = '@vite/client';

    /**
     * @var string The absolute URL to the Vite client.
     */
    public string $viteClientUrl = '';

    /**
     * @var bool
     */
    public bool $useDevServer = false;

    /**
     * @inheritdoc
     */
    public function init(): void
    {
        parent::init();

        $devMode = Craft::$app->getConfig()->getGeneral()->devMode;

        $this->viteClientUrl = $this->buildUrl(
            $this->viteServerUrlPublic,
            $this->viteClientPath
        );

        $this->useDevServer = $devMode && $this->getIsDevServerRunning();
    }

    /**
     * @param $paths
     * @return Markup
     */
    public function auto($paths): Markup
    {
        if ($this->useDevServer) {
            return $this->devTags($paths);
        }

        return $this->productionTags($paths);
    }

    /**
     * Outputs the necessary tags for use with the Vite dev server.
     *
     * @param array|string $paths
     * @return Markup
     */
    public function devTags(array|string $paths): Markup
    {
        $styles = Collection::make();
        $scripts = Collection::make();

        $paths = is_array($paths) ? $paths : [$paths];

        $scripts->add($this->viteClientUrl);

        foreach ($paths as $path) {
            if ($this->isCssPath($path)) {
                $styles->add($this->getDestFileUrl($path));
            } else {
                $scripts->add($this->getDestFileUrl($path));
            }
        }

        $styles = $styles->unique()->filter();
        $scripts = $scripts->unique()->filter();

        $output = [];

        foreach ($styles as $style) {
            $output[] = Html::cssFile($style);
        }

        foreach ($scripts as $script) {
            $output[] = Html::jsFile($script, [
                'type' => 'module',
                'crossorigin' => 'anonymous',
            ]);
        }

        return Template::raw(implode("\r\n", $output));
    }

    /**
     * Outputs the necessary tags for use with the manifest file.
     *
     * @param array|string $paths
     * @return Markup
     */
    public function productionTags(array|string $paths): Markup
    {
        $styles = Collection::make();
        $scripts = Collection::make();
        $preloads = Collection::make();

        $paths = is_array($paths) ? $paths : [$paths];

        foreach ($paths as $path) {
            if ($this->isCssPath($path)) {
                $styles->add($this->getDestFileUrl($path));

                continue;
            }

            $styles->add($this->getStyles($path));
            $preloads->add($this->getImports($path));
            $scripts->add($this->getDestFileUrl($path));
        }

        $styles = $styles->flatten()->unique()->filter();
        $scripts = $scripts->flatten()->unique()->filter();
        $preloads = $preloads->flatten()->unique()->filter();

        $output = [];

        foreach ($styles as $style) {
            $output[] = Html::cssFile($style);
        }

        foreach ($scripts as $script) {
            $output[] = Html::jsFile($script, [
                'type' => 'module',
                'crossorigin' => true,
            ]);
        }

        foreach ($preloads as $preload) {
            $output[] = Html::tag('link', '', [
                'crossorigin' => true,
                'href' => $preload,
                'rel' => 'modulepreload',
            ]);
        }

        return Template::raw(implode("\r\n", $output));
    }

    /**
     * Gets the styles imported within a script.
     *
     * @param string $path
     * @param array $files
     * @return array
     */
    public function getStyles(string $path, array &$files = []): array
    {
        $manifest = $this->getManifest();
        $entry = ArrayHelper::getValue($manifest, $path);

        if ($entry === null) {
            return [];
        }

        foreach ($entry['css'] ?? [] as $import) {
            $files[] = $this->getDestFileUrl($import);

            $this->getImports($import, $files);
        }

        return array_unique($files);
    }

    /**
     * Gets the imported files within a script.
     *
     * @param string $path
     * @param array $files
     * @return array
     */
    public function getImports(string $path, array &$files = []): array
    {
        $manifest = $this->getManifest();
        $entry = ArrayHelper::getValue($manifest, $path);

        if ($entry === null) {
            return [];
        }

        foreach ($entry['imports'] ?? [] as $import) {
            $files[] = $this->getDestFileUrl($import);

            $this->getImports($import, $files);
        }

        return array_unique($files);
    }

    /**
     * Gets the URL to an asset.
     *
     * @see Service::getDestFileUrl()
     * @param string|null $path
     * @return string
     */
    public function getAsset(string $path = null): string
    {
        return $this->getDestFileUrl($path);
    }

    /**
     * If the dev server is running it returns the path with the servers host. When
     * the dev server isn't running it will first try to read the dest path from the
     * manifest file. If the file can't be found in the manifest it will return the
     * raw path. The second behaviour can be used to get the URL for static assets.
     *
     * @param string $path
     * @return string
     */
    public function getDestFileUrl(string $path): string
    {
        if ($this->useDevServer) {
            return $this->buildUrl($this->viteServerUrlPublic, $path);
        }

        $path = trim($path, '/');
        $manifest = $this->getManifest();
        $file = ArrayHelper::getValue($manifest, [$path, 'file'], $path);

        return $this->buildUrl($this->vitePublicUrl, $file);
    }

    public function getDestFilePath($path)
    {
        if ($this->useDevServer) {
            return $this->buildUrl($this->viteServerUrlPublic, $path);
        }

        $path = trim($path, '/');
        $manifest = $this->getManifest();
        $file = ArrayHelper::getValue($manifest, [$path, 'file'], $path);

        return $this->buildUrl($this->vitePublicPath, $file);
    }

    /**
     * Outputs the file contents for the provided path. The file contents are
     * cached using the manifest file as the cache dependency.
     *
     * @param string $path
     * @return string|Markup
     */
    public function inline(string $path): string|Markup
    {
        if (UrlHelper::isAbsoluteUrl($path)) {
            $dest = $path;
        } else {
            $dest = $this->getDestFilePath($path);
        }

        $content = $this->getFileContents($dest);

        if ($content === null) {
            return '';
        }

        return Template::raw($content);
    }

    /**
     * Gets the Vite manifest as array.
     *
     * The manifest file contents are cached using the file as the dependency.
     *
     * @return array
     */
    public function getManifest(): array
    {
        $path = (string)App::parseEnv($this->manifestPath);

        $content = $this->getFileContents($path);

        if (!$content || !Json::isJsonObject($content)) {
            Craft::error('Missing or invalid manifest' . $path, __METHOD__);

            return [];
        }

        return Json::decode($content);
    }

    /**
     * Retrieves the contents of a file.
     *
     * @param $path
     * @return mixed
     */
    public function getFileContents($path): mixed
    {
        $cacheKey = Module::getInstance()->cacheKeyPrefix;
        $pathCacheKey = "{$cacheKey}:{$path}";

        $cacheTagDependency = new TagDependency([
            'tags' => [$cacheKey, $pathCacheKey],
        ]);

        $cacheFileDependency = new FileDependency([
            'fileName' => $path,
        ]);

        $cacheChainedDependency = new ChainedDependency([
            'dependencies' => [$cacheTagDependency, $cacheFileDependency],
        ]);

        $cacheDuration = $this->useDevServer ? $this->devModeCacheDuration : $this->cacheDuration;

        return Craft::$app->getCache()->getOrSet($pathCacheKey, function() use ($path) {
            $contents = null;

            if (UrlHelper::isAbsoluteUrl($path)) {
                $client = Craft::createGuzzleClient([
                    RequestOptions::CONNECT_TIMEOUT => 3,
                    RequestOptions::TIMEOUT => 5,
                    RequestOptions::VERIFY => false,
                ]);

                try {
                    $response = $client->get($path);

                    if ($response->getStatusCode() !== 200) {
                        return null;
                    }

                    $contents = $response->getBody()->getContents();
                } catch (Throwable $e) {
                    Craft::error($e->getMessage(), __METHOD__);
                }
            } else {
                $contents = @file_get_contents($path);
            }

            if ($contents === '') {
                return null;
            }

            return $contents;
        }, $cacheDuration, $cacheChainedDependency);
    }

    /**
     * Joins a given URL and path taking care of any slashes.
     *
     * @param string $url
     * @param string|null $path
     * @return string
     */
    public function buildUrl(string $url, string $path = null): string
    {
        $url = (string)App::parseEnv($url);

        return rtrim($url, '/') . '/' . trim($path, '/');
    }

    public function getIsDevServerRunning(): bool
    {
        $response = null;

        try {
            $response = (new Client())->request('GET', $this->viteClientUrl, [
                RequestOptions::CONNECT_TIMEOUT => 5,
                RequestOptions::HTTP_ERRORS => false,
                RequestOptions::TIMEOUT => 5,
                RequestOptions::VERIFY => false,
            ]);
        } catch (Throwable $exception) {
            Craft::error($exception->getMessage(), __METHOD__);
        }

        if ($response === null || $response->getStatusCode() !== 200) {
            return false;
        }

        return true;
    }

    public function isCssPath(string $path): bool
    {
        return preg_match('/^.*\.(css|less|sass|scss|styl|stylus|pcss|postcss)$/', $path) === 1;
    }
}
