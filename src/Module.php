<?php

namespace nixondesign\vite;

/**
 * @property Service $vite
 */
class Module extends \yii\base\Module
{
    /**
     * @var string The prefix to add to cache tags.
     */
    public string $cacheKeyPrefix = 'vite';

    public function getVite(): Service
    {
        return $this->get('vite');
    }
}
